<?php

/**
 * This is the model class for table "books".
 *
 * The followings are the available columns in table 'books':
 * @property integer $id
 * @property string $book_name
 * @property string $book_image
 * @property string $published_year
 * @property string $book_status
 * @property integer $user_id
 */
class Books extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'books';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('book_name', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('book_name, published_year, book_image, book_status', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
            array('book_image', 'file', 'types'=>'jpg, gif, png',
                //'wrongType'=>'Only .ipg and .png files.',
                'maxSize'=>1024*1024*5,
                'tooLarge'=>'The file was larger than 5MB. Please upload a smaller file.',
                'allowEmpty' => false,
                ),

			array('id, book_name, book_image, published_year, book_status, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                'user'=>array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'book_name' => 'Book Name',
			'book_image' => 'Book Image',
			'published_year' => 'Published Year',
			'book_status' => 'Book Status',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare('id',$this->id);
		$criteria->compare('book_name',$this->book_name,true,'OR');
		//$criteria->compare('book_image',$this->book_image,true);
		//$criteria->compare('published_year',$this->published_year,true);
		$criteria->compare('book_status',$this->book_status,true,'OR');
		//$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>3,
                'pageVar'=>'page',

            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Books the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public static function changeBookStatus()
    {
        return array(0=>'public',1=>'protected',2=>'private');
    }

    public function getNameBookStatus()
    {
        return Books::changeBookStatus()[$this->book_status];
        //return $temp[$this->book_status];
    }


}
