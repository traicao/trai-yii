<?php

class UsersController extends Controller
{
    public function actionIndex()
    {
        $model = new Users;
        if ( isset($_POST['ajax']) && $_POST['ajax']==='users-index-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if ( isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {
                if($model->save())
                {
                    $this->redirect(array('site/login'));
                }
                return;
            }
        }


        $this->render('index',array('model'=>$model));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Users']))
        {
            $model->attributes=$_POST['Users'];
            if($model->save())
                $this->redirect(array('profile'));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionProfile()
    {

        $id =Yii::app()->user->id;
        $condition= 'user.id='.$id;
        $criteria = new CDbCriteria();
        $criteria->with = array('user');
        $criteria->condition=$condition;
       // $criteria->condition=$condition;
        $dataProvider=new CActiveDataProvider('Books', array(
            'criteria'=>$criteria,
        ));

        $this->render('profile',array(
            'dataProvider'=>$dataProvider,
        ));




    }
    public function loadModel($id)
    {
        $model=Users::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


    // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}