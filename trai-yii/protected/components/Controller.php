<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

//    public function init()
//    {
//        $auth=Yii::app()->authManager;
//
//        $auth->createOperation('createBook','create a Book');
//        $auth->createOperation('readBook','read a Book');
//        $auth->createOperation('updateBook','update a Book');
//        $auth->createOperation('deleteBook','delete a Book');
//
//        $bizRule='return Yii::app()->user->id==$params["Book"]->user_id;';
//        $task=$auth->createTask('updateOwnBook','update a Book by author himself',$bizRule);
//        $task->addChild('updateBook');
//
//        $role=$auth->createRole('reader');
//        $role->addChild('readBook');
//
//        $role=$auth->createRole('author');
//        $role->addChild('reader');
//        $role->addChild('createBook');
//        $role->addChild('updateOwnBook');
//
//        $role=$auth->createRole('editor');
//        $role->addChild('reader');
//        $role->addChild('updateBook');
//
//        $role=$auth->createRole('admin');
//        $role->addChild('editor');
//        $role->addChild('author');
//        $role->addChild('deleteBook');
//
//        $auth->assign('reader','readerA');
//        $auth->assign('author','authorB');
//        $auth->assign('editor','editorC');
//        $auth->assign('admin','adminD');
//    }
    public function accessRules() {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('*'),

            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update','download'),
               // 'users'=>array('@'),
                'roles'=>'admin',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
}