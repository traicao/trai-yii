<?php

class BooksController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
	    $rules = parent::accessRules();
        $rules[] = array('allow', // allow authenticated user to perform 'create' and 'update' actions
            'actions'=>array('update'),

            //'users'=>array('@'),
        );
        return $rules;
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Books;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Books']))
		{
            $model->attributes=$_POST['Books'];
            $model->user_id = Yii::app()->user->getId();
            $model->book_image = CUploadedFile::getInstance($model,'book_image');

            if($model->save()) {

                // $path = Yii::app()->basePath . '/'.'uploads/' . $model->book_image;
                $model->book_image->saveAs(Yii::getPathOfAlias('webroot').'/images/uploaded/'.$model->book_image);
                $this->redirect(array('view','id'=>$model->id));
            }

            //Yii::app()->session->setFlash('success', 'You have successfully updated something!');

		}



		$this->render('create',array(
			'model'=>$model,
		));
	}


    function actionDownload(){
        $name = $_GET['filename'];
//	    var_dump($name);
        $filecontent=file_get_contents(Yii::getPathOfAlias('webroot').'/images/uploaded/'.$name);
        header("Content-Type: text/plain");
        header("Content-disposition: attachment; filename=$name");
        header("Pragma: no-cache");
        echo $filecontent;
        exit;
    }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{

		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Books']))
		{
			$model->attributes=$_POST['Books'];
            $params=array('book'=>$model);
            if(Yii::app()->user->checkAccess('updateBook',$params))
            {
                if($model->save())
                    $this->redirect(array('view','id'=>$model->id));
            }
            else{
                var_dump('loi');die();
            }

		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $criteria = new CDbCriteria;
        if (Yii::app()->user->isGuest)
        {
            $criteria->condition = 'book_status = "public" ';
        }
//        else {
//            $criteria->condition = 'book_status ='.'';
//        }

        $dataProvider=new CActiveDataProvider('Books',array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>3,
                'pageVar'=>'page',

            ),
        ));
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Books('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Books']))
			$model->attributes=$_GET['Books'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Books::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='books-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
