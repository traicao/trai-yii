<div class="view">

    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/uploaded/'.$data->book_image); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('book_name')); ?>:</b>
    <?php echo CHtml::encode($data->book_name); ?>
    <br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('published_year')); ?>:</b>
	<?php echo CHtml::encode($data->published_year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('book_status')); ?>:</b>
	<?php echo CHtml::encode($data->getNameBookStatus()); ?>
	<br />

    <?php echo CHtml::link($data->book_image,array('Books/download','filename'=>$data->book_image)); ?>

	<br />


</div>